<%--
  Created by IntelliJ IDEA.
  User: Maciej
  Date: 2016-12-23
  Time: 15:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
    <title>Test</title>
</head>
<body>
    TEST APLIKACJI<br />
<h3>${message}</h3>
<h3>${person}</h3>
<h4>Przykładowy formularz i jego obsługa w Spring</h4>
<div>
    <form:form modelAttribute="newPerson" method="post" action="/getAll">
        Imię:<form:input path="name" id="person_name" type = "text"/><br />
        Nazwisko:<form:input path="lastName" type="text" id="person_lastName"/><br />
        <input type="submit" id="addButton" value="Dodaj"/>
    </form:form>
</div>
</body>
</html>
