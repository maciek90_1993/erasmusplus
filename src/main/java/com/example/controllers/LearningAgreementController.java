package com.example.controllers;

import com.example.domain.type.StatusType;
import com.example.repository.LearningAgreementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by limack on 25.01.17.
 */
@Controller
public class LearningAgreementController {

    @Autowired
    private LearningAgreementRepository learningAgreementRepository;

    @RequestMapping(value = "/learning-agreement", method = RequestMethod.GET)
    public String showLerningAgreement(Model model){
        model.addAttribute("learningAgreementsToCheck", learningAgreementRepository.getAllByStatus(StatusType.TO_CHECK));
        model.addAttribute("learningAgreementsAccepted", learningAgreementRepository.getAllByStatus(StatusType.ACCEPT));
        model.addAttribute("learningAgreementsDeclined", learningAgreementRepository.getAllByStatus(StatusType.DECLINE));
        return "learning-agreement";
    }

    @RequestMapping(value = "/learning-agreement/accept", method = RequestMethod.POST)
    public String  acceptLearningAgreement(@RequestParam Long idAgreement, Model model){
        learningAgreementRepository.setStatusLearningAgreement(idAgreement, StatusType.ACCEPT);
        return "learning-agreement";
    }

    @RequestMapping(value = "/learning-agreement/decline", method = RequestMethod.POST)
    public String declineLearningAgreement(@RequestParam Long idAgreement, Model model){
        learningAgreementRepository.setStatusLearningAgreement(idAgreement, StatusType.DECLINE);
        return "learning-agreement";
    }

}
