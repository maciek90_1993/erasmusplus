package com.example.controllers;

import com.example.domain.type.AlertType;
import com.example.domain.Person;
import com.example.domain.SuperUser;
import com.example.repository.PersonRepository;
import com.example.to.UserTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by limack on 03.01.17.
 */
@Controller
@Scope("session")
public class LoginController {

    @Autowired
    public PersonRepository personRepository;

    public static final String CURRENT_USER = "currentUser";

    @RequestMapping(value = "/login")
    public String login(Model model){
        model.addAttribute("loginPerson", new Person());
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute("loginPerson") UserTO userTO, Model model,
                        HttpServletRequest request){

        SuperUser superUser = personRepository.findUserByLoginAndPassword(userTO);
        System.out.println("role: " + userTO.getRole());
        if(superUser.getLogin() == null) {
            showAlert("Incorrect login or password", AlertType.FAIL, model);
            return "login";
        }

        UserTO user = buildUserTo(superUser, userTO.getRole());

        request.getSession().setAttribute(CURRENT_USER, user);
        model.addAttribute("user", superUser);
        return "index";
    }

    private UserTO buildUserTo(SuperUser user, String role) {
        UserTO userTo = new UserTO();
        userTo.setId(user.getId());
        userTo.setLogin(user.getLogin());
        userTo.setRole(role);
        return userTo;
    }

    @RequestMapping(value = {"/logout"}, method = RequestMethod.GET)
    public String notLogged(Model model, HttpServletRequest request) {
        request.getSession().removeAttribute(CURRENT_USER);
        model.addAttribute("loginPerson", new Person());
        return "login";
    }

    private void showAlert(String message, AlertType alertType, Model model) {
        model.addAttribute("message", message);
        model.addAttribute("alertType", alertType);
    }

}
