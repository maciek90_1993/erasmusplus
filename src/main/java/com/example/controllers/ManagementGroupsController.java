package com.example.controllers;

import com.example.domain.*;
import com.example.repository.ListCoursesRepository;
import com.example.repository.ListDepartmentRepository;
import com.example.repository.OffertsDepartmentRepository;
import com.example.repository.SubjectsRepository;
import com.example.service.ManagmentGroupsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Maciej on 2017-01-04.
 */

@Controller
@RequestMapping(value = "/Groups")
public class ManagementGroupsController {

    @Autowired
    private SubjectsRepository subjectsRepository;
    @Autowired
    private ListCoursesRepository listCoursesRepository;
    @Autowired
    private ListDepartmentRepository listDepartmentRepository;
    @Autowired
    private OffertsDepartmentRepository offertsDepartmentRepository;
    @Autowired
    private ManagmentGroupsService managmentGroupsService;


    private List<Subjects> subjectsList;
    private List<ListCourses>listCourses;
    private List<ListDepartment>listDepartments;
    private List<OffertsDepartment>offertsDepartments;
    private List<ListGroups>listGroupses;

    @RequestMapping
    public String index(Model model){
       subjectsList = subjectsRepository.getAllSubjects();
       listCourses = managmentGroupsService.getAllCourses();

        Map<String, List<OffertsDepartment>>map = new HashMap<String, List<OffertsDepartment>>();
        Map<String, Integer> amountStudents = new HashMap<String, Integer>();
        amountStudents = managmentGroupsService.amountStudentsForOneSubject();
        map  = managmentGroupsService.showSubjectFromCourse();

        model.addAttribute("subjects", map);
        model.addAttribute("courses", listCourses);
        model.addAttribute("amountsStudent", amountStudents);
        return "management_groups";
    }

    @RequestMapping(value = "/createGroup", method = RequestMethod.POST )
    @ResponseBody
    public String createGroups(@RequestParam int idCourse, @RequestParam int idSubject){
        System.out.println(idCourse + " "+ idSubject);
        managmentGroupsService.createGroups(idCourse, idSubject);
        return "management_groups";
    }

    @RequestMapping(value="/listGroups")
    public String showListGroups(Model model){
        listGroupses = managmentGroupsService.fetchAllGroups();
        model.addAttribute("listGroups", listGroupses);
        return "groups_list_view";
    }

    @RequestMapping(value = "/deleteGroup", method = RequestMethod.POST)
    public String  deteleGroup(@RequestParam int idGroup, Model model){
        System.out.println(idGroup);
        ListGroups listGroups = managmentGroupsService.findGroupById(idGroup);
        managmentGroupsService.deleteSelectedGroup(idGroup);
        return "groups_list_view";    }

    @RequestMapping(value="/checkWhichGroupsWasCreate", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public List<ListGroups> checkWhichGroupsWasCreated(){
        listGroupses = managmentGroupsService.fetchAllGroups();
        return listGroupses;
    }

    @RequestMapping (value = "/deleteStudentFromGroup", method = RequestMethod.POST)
    public String deleteStudentFromGroup(@RequestParam int idGroup, @RequestParam int idStudent){
        managmentGroupsService.removeStudentFromGroup(idGroup, idStudent);
        return "groups_list_view";
    }

    @RequestMapping(value = "/addToGroup", method = RequestMethod.GET)
    public String addStudentToGroup(@RequestParam int idGroup, @RequestParam String nameStudent, @RequestParam String surnameStudent){
        System.out.print(idGroup+" "+nameStudent+" "+surnameStudent);
        managmentGroupsService.addStudentGroup(idGroup, nameStudent, surnameStudent);
        return "groups_list_view";
    }

}
