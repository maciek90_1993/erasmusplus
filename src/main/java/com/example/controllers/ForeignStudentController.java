package com.example.controllers;

/**
 * Created by Maciej on 2017-01-12.
 */
import com.example.domain.ForeignStudent;
import com.example.domain.LearningAgreement;
import com.example.domain.Subjects;
import com.example.domain.type.StatusType;
import com.example.repository.ForeignStudentRepository;
import com.example.repository.LearningAgreementRepository;
import com.example.repository.OffertsDepartmentRepository;
import com.example.to.UserTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


@Controller
public class ForeignStudentController {

    public static final String CURRENT_USER = "currentUser";
    @Autowired
    private ForeignStudentRepository foreignStudentRepository;

    @Autowired
    private LearningAgreementRepository learningAgreementRepository;
    @Autowired
    private OffertsDepartmentRepository offertsDepartmentRepository;

    List<ForeignStudent>list;
    @RequestMapping(value = "/students")
    public String showAllForeignStudents(Model model  ,HttpServletRequest request){
        list = foreignStudentRepository.getAllForeignStudents();

        UserTO currentUser = (UserTO) request.getSession().getAttribute(CURRENT_USER);
        ForeignStudent studentLogIn = foreignStudentRepository.findStudentById(currentUser.getId());
        model.addAttribute("currentStudent", studentLogIn);

        return "foreignstudent";
    }

    @RequestMapping(value="/addLA", method = RequestMethod.POST)
    public String addLearningAgreement(@RequestParam String nameStudent, @RequestParam String surnameStudent,
                                       @RequestParam String adressStudent, @RequestParam String academyYear,
                                       @RequestParam String dateBirth, @RequestParam String country,
                                       @RequestParam String codeSubjects, @RequestParam String department,
                                       @RequestParam String language, @RequestParam String nationality,
                                       @RequestParam String phone, @RequestParam Character sex, HttpServletRequest request){

        UserTO currentUser = (UserTO) request.getSession().getAttribute(CURRENT_USER);
        ForeignStudent foreignStudent = foreignStudentRepository.findStudentById(currentUser.getId());
        LearningAgreement la = new LearningAgreement();
        la.setName(nameStudent);
        la.setLastname(surnameStudent);
        la.setAddress(adressStudent);
        la.setAcademyYear(academyYear);
        DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
        Date date = new Date();
        try {
            date = format.parse(dateBirth);
            la.setDateOfBirth(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        la.setCountry(country);
        la.setSubjectAreaCode(codeSubjects);
        la.setDepartnent(department);
        la.setLangaugeCompentence(language);
        la.setNationality(nationality);
        la.setPhone(phone);
        la.setSex(sex);
        la.setForeignStudent(foreignStudent);
        la.setStatus(StatusType.TO_CHECK);
        learningAgreementRepository.addLearningAgreement(la);

        return "foreignstudent";
    }

    @RequestMapping(value = "/checkCreateLA", method = RequestMethod.GET, headers = "Accept=application/json")
    @ResponseBody
    public LearningAgreement checkThatUserHaveLa(HttpServletRequest request){
        UserTO currentUser = (UserTO) request.getSession().getAttribute(CURRENT_USER);
        LearningAgreement la=learningAgreementRepository.findLearningAgrementByIdForeignStudent(currentUser.getId());
        return la;
    }
    @RequestMapping(value="/showListSubject")
    public String showListSubjectToCourseStudent(Model model, HttpServletRequest request){
        UserTO currentUser = (UserTO) request.getSession().getAttribute(CURRENT_USER);
        ForeignStudent student = foreignStudentRepository.findStudentById(currentUser.getId());
        List<Subjects>subjectsList = offertsDepartmentRepository.fetchAllSubjectFromDepartmentOferts(student.getListCourses().getIdCourse());
        model.addAttribute("subjectList", subjectsList);
        return "listSubjectForForgeinStudent";
    }

    @RequestMapping(value="/updateLaCodeSubjects", method=RequestMethod.POST)
    public String updateLaCodeSubjects(@RequestParam String  subject1, @RequestParam String  subject2, @RequestParam String  subject3,
                                       HttpServletRequest request  ){

        String conectString = subject1+", "+subject2+", "+subject3;
        UserTO currentUser = (UserTO) request.getSession().getAttribute(CURRENT_USER);
        LearningAgreement la=learningAgreementRepository.findLearningAgrementByIdForeignStudent(currentUser.getId());
        learningAgreementRepository.updateListSubjectInLearninEgrement(conectString, la.getId());
        return "listSubjectForForgeinStudent";
    }
    @RequestMapping(value = "/updataDataUser", method = RequestMethod.POST)
    public String updateDataUser(@RequestParam String nameStudent, @RequestParam String surnameStudent, @RequestParam String loginStudent,
                                 HttpServletRequest request   ){
        UserTO currentUser = (UserTO) request.getSession().getAttribute(CURRENT_USER);
        ForeignStudent currentStudent = foreignStudentRepository.findStudentById(currentUser.getId());
        foreignStudentRepository.updataDataStudent(currentStudent.getId(), nameStudent, surnameStudent, loginStudent);

        return "foreignstudent";
    }
}
