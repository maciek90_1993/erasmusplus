package com.example.controllers;

import com.example.domain.Person;
import com.example.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by Maciej on 2016-12-23.
 */
@Controller
@RequestMapping(value="/")
@Scope("session")
public class HomeController {

    @Autowired
    public PersonRepository personRepository;
    private Person a;

    @RequestMapping
    public String home(Model model){
        model.addAttribute("message", "Maciej tu był :D !!!");
        Person newPerson = new Person();
        model.addAttribute("newPerson", newPerson);
        return "test";
    }
    @RequestMapping(value = "/index")
    public String index(Model model, HttpServletRequest request){
        Person personFromSession = (Person) request.getAttribute("currentUser");

        model.addAttribute("currentUser", personFromSession);
        model.addAttribute("message", "Maciej tu był :D !!!");
        return "index";
    }
    @RequestMapping(value="/findAll")
    public String findAllPerson(Model model){
        return "login";
    }

    @RequestMapping(value="/getAllPerson")
    public String getAll(Model model){
        model.addAttribute("person", personRepository.getAll());
        Person newPerson = new Person();
        model.addAttribute("newPerson", newPerson);
        return "test";
    }

    @RequestMapping(value = "/getAll" ,method= RequestMethod.GET)
    public String getAddNewPerson(Model model){
        Person newPerson = new Person();
        model.addAttribute("newPerson", newPerson);
        return "test";
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.POST)
    public String addNewPerson(@ModelAttribute("newPerson") Person personAdded){
        personRepository.addPerson(personAdded);
        return "redirect:/";
    }
    @RequestMapping(value="/erw")
    public String test(Model model){
        model.addAttribute("message", ",aee");
        return "test";

    }
}
