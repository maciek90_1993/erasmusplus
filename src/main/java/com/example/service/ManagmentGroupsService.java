package com.example.service;

import com.example.domain.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Maciej on 2017-01-09.
 */
public interface ManagmentGroupsService {
    Map<String, List<OffertsDepartment>> showSubjectFromCourse();
    public List<ListCourses> getAllCourses();
    public Map<String, Integer>amountStudentsForOneSubject();
    public void createGroups(int idCourse, int idSubject);
    public List<ListGroups>fetchAllGroups();
    public void deleteSelectedGroup(int idGroup);
    public ListGroups findGroupById(int idGroup);
    public void removeStudentFromGroup(int idGroup, int idStudent);
    public void addStudentGroup(int idGroup, String nameStudent, String surnameStudent);
}
