package com.example.domain;

import javax.persistence.*;

/**
 * Created by Maciej on 2017-01-12.
 */
@Entity
@Table(name = "foreignstudent")
public class ForeignStudent implements SuperUser {

    @Id
    private Long id;
    private String firstName;
    private String lastName;
    private String address;

    @OneToOne
    @JoinColumn(name="id_kierunku")
    private ListCourses listCourses;

    @ManyToOne
    @JoinColumn(name = "id_group")
    private ListGroups group;

    private String login;
    private String password;
    public ListCourses getListCourses() {
        return listCourses;
    }


    public ListGroups getGroup() {
        return group;
    }

    public void setGroup(ListGroups group) {
        this.group = group;
    }

    public void setListCourses(ListCourses listCourses) {
        this.listCourses = listCourses;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }



    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
