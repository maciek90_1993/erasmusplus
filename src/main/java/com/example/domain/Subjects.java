package com.example.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by Maciej on 2017-01-04.
 */

@Entity
@Table(name = "przedmioty")
public class Subjects {

    @Id
    //@GeneratedValue(generator="increment")
    //@GenericGenerator(name="increment", strategy = "increment")
    @GeneratedValue
    @Column(name = "id_przedmiotu")
    private int id;

    @Column(name="kod")
    private String cod;

    @Column(name="nazwa")
    private String name;

    @Column(name="semestr")
    private String term;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Override
    public String toString(){
        return "Przedmiot:"+cod+" nazwa: "+name+" semestr: "+term;
    }
}
