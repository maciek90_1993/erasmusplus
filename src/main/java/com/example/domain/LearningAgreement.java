package com.example.domain;

import com.example.domain.type.StatusType;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by limack on 25.01.17.
 */
@Entity
public class LearningAgreement {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER)
    private ForeignStudent foreignStudent;

    private String name;
    private String lastname;
    private Date dateOfBirth;
    private String nationality;
    private Character sex;
    private String academyYear;
    private String langaugeCompentence;
    private Character studyCycle;
    private String subjectAreaCode;
    private String phone;
    private String email;
    private String nameSending;
    private String nameReceiving;
    private String faculty;
    private String address;
    private String personContactName;
    private String departnent;
    private String country;
    private StatusType status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ForeignStudent getForeignStudent() {
        return foreignStudent;
    }

    public void setForeignStudent(ForeignStudent foreignStudent) {
        this.foreignStudent = foreignStudent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Character getSex() {
        return sex;
    }

    public void setSex(Character sex) {
        this.sex = sex;
    }

    public String getAcademyYear() {
        return academyYear;
    }

    public void setAcademyYear(String academyYear) {
        this.academyYear = academyYear;
    }

    public String getLangaugeCompentence() {
        return langaugeCompentence;
    }

    public void setLangaugeCompentence(String langaugeCompentence) {
        this.langaugeCompentence = langaugeCompentence;
    }

    public Character getStudyCycle() {
        return studyCycle;
    }

    public void setStudyCycle(Character studyCycle) {
        this.studyCycle = studyCycle;
    }

    public String getSubjectAreaCode() {
        return subjectAreaCode;
    }

    public void setSubjectAreaCode(String subjectAreaCode) {
        this.subjectAreaCode = subjectAreaCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNameSending() {
        return nameSending;
    }

    public void setNameSending(String nameSending) {
        this.nameSending = nameSending;
    }

    public String getNameReceiving() {
        return nameReceiving;
    }

    public void setNameReceiving(String nameReceiving) {
        this.nameReceiving = nameReceiving;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPersonContactName() {
        return personContactName;
    }

    public void setPersonContactName(String personContactName) {
        this.personContactName = personContactName;
    }

    public String getDepartnent() {
        return departnent;
    }

    public void setDepartnent(String departnent) {
        this.departnent = departnent;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public StatusType getStatus() {
        return status;
    }

    public void setStatus(StatusType statusType) {
        this.status = statusType;
    }

    @Override
    public String toString() {
        return "LearningAgreement{" +
                "id=" + id +
                ", foreignStudent=" + foreignStudent +
                ", name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", nationality='" + nationality + '\'' +
                ", sex=" + sex +
                ", academyYear='" + academyYear + '\'' +
                ", langaugeCompentence='" + langaugeCompentence + '\'' +
                ", studyCycle=" + studyCycle +
                ", subjectAreaCode='" + subjectAreaCode + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", nameSending='" + nameSending + '\'' +
                ", nameReceiving='" + nameReceiving + '\'' +
                ", faculty='" + faculty + '\'' +
                ", address='" + address + '\'' +
                ", personContactName='" + personContactName + '\'' +
                ", departnent='" + departnent + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
