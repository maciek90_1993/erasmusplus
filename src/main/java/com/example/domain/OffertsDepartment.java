package com.example.domain;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Maciej on 2017-01-05.
 */
@Entity
@Table(name="oferta_wydzialu")
public class OffertsDepartment extends Object {

    @Id
    @GeneratedValue
    @Column(name="id_oferty")
    private Long idOfferts;
    @OneToOne
    @JoinColumn(name="id_wydzialu")
    private ListDepartment department;
    @OneToOne
    @JoinColumn(name="id_kierunku")
    private ListCourses courses;

    @OneToOne
    @JoinColumn(name="id_przedmiotu")
    private Subjects subjects;

    public Long getIdOfferts() {
        return idOfferts;
    }

    public void setIdOfferts(Long idOfferts) {
        this.idOfferts = idOfferts;
    }

    public ListDepartment getDepartment() {
        return department;
    }

    public void setDepartment(ListDepartment department) {
        this.department = department;
    }

    public ListCourses getCourses() {
        return courses;
    }

    public void setCourses(ListCourses courses) {
        this.courses = courses;
    }

    public Subjects getSubjects() {
        return subjects;
    }

    public void setSubjects(Subjects subjects) {
        this.subjects = subjects;
    }




}
