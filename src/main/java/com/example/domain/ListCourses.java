package com.example.domain;

import javax.persistence.*;

/**
 * Created by Maciej on 2017-01-04.
 */
@Entity
@Table(name="lista_kierunkow")

public class ListCourses {
    @Id
    @Column(name = "id_kierunku")
    @GeneratedValue
    private int idCourse;

    @Column(name = "nazwa")
    private String nameCourse;

    @Column(name = "semestr_rozp")
    private String termStartCourse;

    @Column(name ="stopien_studiow")
    private String courseDegree;



    public int getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(int idCourse) {
        this.idCourse = idCourse;
    }

    public String getNameCourse() {
        return nameCourse;
    }

    public void setNameCourse(String nameCourse) {
        this.nameCourse = nameCourse;
    }

    public String getTermStartCourse() {
        return termStartCourse;
    }

    public void setTermStartCourse(String termStartCourse) {
        this.termStartCourse = termStartCourse;
    }

    public String getCourseDegree() {
        return courseDegree;
    }

    public void setCourseDegree(String courseDegree) {
        this.courseDegree = courseDegree;
    }

    @Override
    public String toString(){
        return "Kierunek: "+nameCourse+" semestr rozp: "+termStartCourse+ "stopien studiow: "+courseDegree;
    }
}
