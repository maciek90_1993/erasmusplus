package com.example.domain.type;

/**
 * Created by limack on 28.01.17.
 */
public enum StatusType {
    TO_CHECK(0), ACCEPT(1), DECLINE(2);

    private final int id;
    StatusType(int id) { this.id = id; }
    public int getValue() { return id; }

}
