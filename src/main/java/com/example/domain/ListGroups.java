package com.example.domain;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Maciej on 2017-01-16.
 */
@Entity

public class ListGroups {
    @Id
    @GeneratedValue
    private int idGroup;
    @OneToOne
    private ListCourses course;
    @OneToOne
    private Subjects subject;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "Groups_has_students")
   // @JoinColumn(name="id_students")
    private List<ForeignStudent> foreignStudentList;



    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public ListCourses getCourse() {
        return course;
    }

    public void setCourse(ListCourses course) {
        this.course = course;
    }

    public Subjects getSubject() {
        return subject;
    }

    public void setSubject(Subjects subject) {
        this.subject = subject;
    }

    public List<ForeignStudent> getForeignStudentList() {
        return foreignStudentList;
    }

    public void setForeignStudentList(List<ForeignStudent> foreignStudentList) {
        this.foreignStudentList = foreignStudentList;

    }
}
