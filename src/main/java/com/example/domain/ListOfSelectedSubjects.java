package com.example.domain;

import javax.persistence.*;

/**
 * Created by Maciej on 2017-01-12.
 */

@Entity
@Table(name="lista_wybranych_przedmiotow")
public class ListOfSelectedSubjects {

    @Id
    @Column(name = "id_listy_wyb_prze")
    private int id_listOfSelectedSubjects;

    @OneToOne
    @JoinColumn(name="id_przedmiotu")
    private Subjects subject;
    @OneToOne
    @JoinColumn(name="id_studenta")
    private ForeignStudent foreignStudent;


    public int getId_listOfSelectedSubjects() {
        return id_listOfSelectedSubjects;
    }

    public void setId_listOfSelectedSubjects(int id_listOfSelectedSubjects) {
        this.id_listOfSelectedSubjects = id_listOfSelectedSubjects;
    }

    public Subjects getSubject() {
        return subject;
    }

    public void setSubject(Subjects subject) {
        this.subject = subject;
    }
}
