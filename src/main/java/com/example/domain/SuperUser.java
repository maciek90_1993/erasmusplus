package com.example.domain;

/**
 * Created by limack on 22.01.17.
 */
public interface SuperUser {

    Long getId();
    String getLogin();
    String getPassword();
}
