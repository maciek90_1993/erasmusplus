package com.example.domain;

import javax.persistence.*;

/**
 * Created by Maciej on 2017-01-05.
 */
@Entity
@Table(name="lista_wydzialow")
public class ListDepartment {

    @Id
    @Column(name="id_wydzialu")
    @GeneratedValue
    private int idDepartment;

    @Column(name="nazwa")
    private String nameDepartment;

    public int getIdDepartment() {
        return idDepartment;
    }

    public void setIdDepartment(int idDepartment) {
        this.idDepartment = idDepartment;
    }

    public String getNameDepartment() {
        return nameDepartment;
    }

    public void setNameDepartment(String nameDepartment) {
        this.nameDepartment = nameDepartment;
    }

    @Override
    public String toString(){
        return "Wydzial: "+nameDepartment;
    }
}
