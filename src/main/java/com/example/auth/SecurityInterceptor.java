package com.example.auth;

import com.example.controllers.LoginController;
import com.example.to.UserTO;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by limack on 09.01.17.
 */
@Scope("session")
public class SecurityInterceptor implements HandlerInterceptor  {

    private static Logger logger = LoggerFactory.logger(SecurityInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {

        if(!request.getRequestURI().equals("/login")) {
            try {
                UserTO currentUser = (UserTO) request.getSession().getAttribute(LoginController.CURRENT_USER);
                if(currentUser == null) {
                    logger.info("Access denied.");
                    response.sendRedirect("/login");
                }
            }catch(Exception e){
                logger.error("Cannot get user from session ", e);
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

}