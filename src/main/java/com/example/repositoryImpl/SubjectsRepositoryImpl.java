package com.example.repositoryImpl;

import com.example.domain.Subjects;
import com.example.repository.SubjectsRepository;
import org.springframework.jca.cci.core.InteractionCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Maciej on 2017-01-04.
 */
@Repository
@Transactional
public class SubjectsRepositoryImpl implements SubjectsRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<Subjects> getAllSubjects()throws NoResultException {
        //List<Subjects>subjectsList = new ArrayList<Subjects>();
          //  TypedQuery query = entityManager.createQuery("SELECT s FROM Subjects s", Subjects.class);
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();

        CriteriaQuery<Subjects> q = cb.createQuery(Subjects.class);
        Root<Subjects> c = q.from(Subjects.class);
        q.select(c);
        try{
            TypedQuery<Subjects> query = entityManager.createQuery(q);
            List<Subjects>subjectsList =  query.getResultList();
            return subjectsList;
        }catch(NoResultException e){
            return  null;
        }

    }

    @Override
    public Subjects getSubjectById(int idSubject) {
        Query query = entityManager.createQuery("SELECT s FROM Subjects s WHERE s.id=:id");
        query.setParameter("id", idSubject);
        Subjects subjects = null;

        try {
            subjects =(Subjects) query.getSingleResult();
        }catch (NoResultException e){
            e.printStackTrace();
        }
        return subjects;
    }
}
