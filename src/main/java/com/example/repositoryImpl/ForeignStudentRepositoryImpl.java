package com.example.repositoryImpl;

import com.example.domain.ForeignStudent;
import com.example.repository.ForeignStudentRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Piotr Łaskawski on 10.01.2017.
 */
@Repository
@Transactional
public class ForeignStudentRepositoryImpl implements ForeignStudentRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public List<ForeignStudent> getAllForeignStudents() throws NoResultException {
        List<ForeignStudent> foreignStudents;
        Query query = entityManager.createQuery("SELECT l FROM ForeignStudent l");
        try {
            foreignStudents = query.getResultList();
        } catch (NoResultException e) {
            e.getMessage();
            foreignStudents = null;
        }

        return foreignStudents;
    }

    @Override
    public ForeignStudent findStudentById(long idStudent) {
        Query query = entityManager.createQuery("SELECT f FROM ForeignStudent f WHERE f.id=:id");
        query.setParameter("id", idStudent);
        ForeignStudent student = null;

        try {
            student =(ForeignStudent) query.getSingleResult();
        }catch (NoResultException e){
            e.printStackTrace();
        }
        return student;
    }

    @Override
    public ForeignStudent findStudentByNameAndSurname(String name, String surname) {
        Query query = entityManager.createQuery("SELECT f FROM ForeignStudent f WHERE f.firstName=:name AND f.lastName=:surname");
        query.setParameter("name", name);
        query.setParameter("surname",surname);
        ForeignStudent student = null;

        try {
            student =(ForeignStudent) query.getSingleResult();
        }catch (NoResultException e){
            e.printStackTrace();
        }
        return student;
    }

    @Override
    public void updataDataStudent(long idStudent, String name, String surname, String login) {
        ForeignStudent student = entityManager.find(ForeignStudent.class, idStudent);
        if(!name.equals("")){
            student.setFirstName(name);
        }
        if(!surname.equals("")){
            student.setLastName(surname);
        }
        if(!login.equals("")){
            student.setLogin(login);
        }
        entityManager.merge(student);
    }

}
