package com.example.repositoryImpl;

import com.example.domain.LearningAgreement;
import com.example.domain.type.StatusType;
import com.example.repository.LearningAgreementRepository;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by limack on 25.01.17.
 */
@Repository
@Transactional
public class LearningAgreementRepositoryImpl implements LearningAgreementRepository {

    private static Logger logger = LoggerFactory.logger(LearningAgreementRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<LearningAgreement> getAll() {
        Query query = entityManager.createQuery("SELECT la FROM LearningAgreement la");
        try{
            return query.getResultList();
        }catch (NoResultException e){
            logger.info("Not found any result");
            return null;
        }
    }

    @Override
    public List<LearningAgreement> getAllByStatus(StatusType status) {
        Query query = entityManager.createQuery("SELECT la FROM LearningAgreement la WHERE la.status=:status");
        query.setParameter("status", status);
        try{
            return query.getResultList();
        }catch (NoResultException e){
            logger.info("Not found any result");
            return null;
        }
    }

    @Override
    public void setStatusLearningAgreement(Long id, StatusType statusType) {
        LearningAgreement learningAgreement = entityManager.find(LearningAgreement.class, id);
        learningAgreement.setStatus(statusType);
        entityManager.merge(learningAgreement);
    }

    @Override
    public void addLearningAgreement(LearningAgreement la) {
        entityManager.persist(la);
    }

    @Override
    public LearningAgreement findLearningAgrementByIdForeignStudent(Long id) {
        Query query = entityManager.createQuery("SELECT la FROM LearningAgreement la WHERE la.foreignStudent.id=:studentId");
        query.setParameter("studentId", id);
        try{
            LearningAgreement la= (LearningAgreement) query.getSingleResult();
            return la;
        }catch (NoResultException e){
            logger.info("Not found any result");
            return null;
        }
    }

    @Override
    public void updateListSubjectInLearninEgrement(String codesSubjects, long idLa) {
        LearningAgreement la = entityManager.find(LearningAgreement.class, idLa);
        la.setSubjectAreaCode(codesSubjects);
        entityManager.merge(la);
    }




}
