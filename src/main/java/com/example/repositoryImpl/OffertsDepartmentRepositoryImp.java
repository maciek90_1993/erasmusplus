package com.example.repositoryImpl;

import com.example.domain.ListDepartment;
import com.example.domain.OffertsDepartment;
import com.example.domain.Subjects;
import com.example.repository.OffertsDepartmentRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Maciej on 2017-01-05.
 */
@Repository
@Transactional
public class OffertsDepartmentRepositoryImp implements OffertsDepartmentRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<OffertsDepartment> getAllOffertDepartment() throws NoResultException  {
        List<OffertsDepartment>OffertDepartment = null;
        Query query = entityManager.createQuery("SELECT o FROM OffertsDepartment o");

        try{
            OffertDepartment = query.getResultList();
        }catch (NoResultException e){
            OffertDepartment=null;
        }
        return OffertDepartment;
    }

    public List<Subjects>fetchAllSubjectFromDepartmentOferts( int idCourses)throws NoResultException{
        List<Subjects>result=null;

        Query query = entityManager.createQuery("SELECT d FROM OffertsDepartment d WHERE d.courses.idCourse=:idCourse");
       // Query query = entityManager.createQuery("SELECT d FROM OffertsDepartment d ORDER BY d.courses.idCourse");

        //query.setParameter("idDepartment", idDepartment);
        query.setParameter("idCourse", idCourses);


        try{
            result = query.getResultList();
        }catch (NoResultException e){
            result=null;
        }
        return result;
    }


    public List<OffertsDepartment>fetchAllSubjectOnlyFromDepartmentOferts( int idCourses)throws NoResultException{
        List<OffertsDepartment>result=null;

        Query query = entityManager.createQuery("SELECT d FROM OffertsDepartment d WHERE d.courses.idCourse=:idCourse");
        // Query query = entityManager.createQuery("SELECT d FROM OffertsDepartment d ORDER BY d.courses.idCourse");

        //query.setParameter("idDepartment", idDepartment);
        query.setParameter("idCourse", idCourses);


        try{
            result = query.getResultList();

        }catch (NoResultException e){
            result=null;
        }
        return result;
    }
}
