package com.example.repositoryImpl;

import com.example.domain.ForeignStudent;
import com.example.domain.ListCourses;
import com.example.domain.ListGroups;
import com.example.domain.Subjects;
import com.example.repository.ListGroupsRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maciej on 2017-01-16.
 */

@Repository
@Transactional
public class ListGroupsRepositoryImpl implements ListGroupsRepository {


    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void addGroups(ListCourses course, Subjects subjects, List<ForeignStudent> foreignStudentList) {

        ListGroups listGroups = new ListGroups();
        listGroups.setCourse(course);
        listGroups.setSubject(subjects);
        listGroups.setForeignStudentList(foreignStudentList);
        entityManager.persist(listGroups);

    }

    public List<ListGroups>fetchAllGroups()throws NoResultException{
        List<ListGroups>listGroupses = new ArrayList<ListGroups>();
       // Query query = entityManager.createQuery("SELECT g FROM ListGroups g WHERE g.idGroup=:id");
        Query query = entityManager.createQuery("SELECT g FROM ListGroups g");
        //query.setParameter("id", 12);

        try{
            //listGroupses.add( (ListGroups) query.getSingleResult());
            listGroupses = query.getResultList();
        }catch (NoResultException e){
            listGroupses=null;
        }
        return listGroupses;
    }

    @Override
    public void deleteSelectedGroupById(int idGroup) {
        ListGroups group = findGroupById(idGroup);
        entityManager.remove(group);
    }

    @Override
    public ListGroups findGroupById(int idGroup)throws NoResultException {
        Query query = entityManager.createQuery("SELECT g FROM ListGroups g WHERE g.idGroup=:id");
        query.setParameter("id", idGroup);
        ListGroups listGroups = null;
        try{
            listGroups = (ListGroups) query.getSingleResult();
        }catch(NoResultException e){
            e.printStackTrace();
        }
        return listGroups;
    }



    @Override
    public void updateListStudentsInGroup(int idGroup, int idStudent) {
        ListGroups group = entityManager.find(ListGroups.class, idGroup);
        List<ForeignStudent>students = group.getForeignStudentList();
        for (ForeignStudent item: students) {
            if(item.getId() == idStudent){
                students.remove(item);
                break;
            }
        }
        group.setForeignStudentList(students);
        entityManager.merge(group);
    }

    public void addStudentToGroup(int idGroup, ForeignStudent student){
        ListGroups group = entityManager.find(ListGroups.class, idGroup);
        List<ForeignStudent>studentList = group.getForeignStudentList();
        studentList.add(student);
        group.setForeignStudentList(studentList);
        entityManager.merge(group);
    }
}
