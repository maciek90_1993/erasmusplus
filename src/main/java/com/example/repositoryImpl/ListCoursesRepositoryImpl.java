package com.example.repositoryImpl;

import com.example.domain.ListCourses;
import com.example.repository.ListCoursesRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Maciej on 2017-01-05.
 */

@Repository
@Transactional
public class ListCoursesRepositoryImpl implements ListCoursesRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<ListCourses> getAllCourses()throws NoResultException {
        List<ListCourses>listCourses;
        Query query = entityManager.createQuery("SELECT l FROM ListCourses l");

        try{
          listCourses = query.getResultList();
        }catch (NoResultException e){
            listCourses=null;
        }
        return listCourses;
    }

    @Override
    public ListCourses getCourseById(int idCourse) throws NoResultException {

        Query query = entityManager.createQuery("SELECT l FROM ListCourses l WHERE l.idCourse=:id");
        query.setParameter("id", idCourse);
        ListCourses course = null;

        try {
            course =(ListCourses) query.getSingleResult();
        }catch (NoResultException e){
            e.printStackTrace();
        }
        return course;
    }
}
