package com.example.repositoryImpl;

import com.example.domain.ForeignStudent;
import com.example.domain.Subjects;
import com.example.repository.ListOfSelectedSubjectsRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Maciej on 2017-01-12.
 */

@Repository
@Transactional
public class ListOfSelectedSubjectsRepositoryImpl implements ListOfSelectedSubjectsRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Subjects> getAllListOfSelectedSubjectsForOneStudents(String id_Students) {
        return null;
    }


    public int getHowManyStudentsSelectedOneSubjectsFromOneCourse(int id_course, int id_subject){

        List<Subjects>result = null;
        Query query = entityManager.createQuery("SELECT l FROM ListOfSelectedSubjects l WHERE l.subject.id=:idSubject AND l.foreignStudent.listCourses.idCourse=:idCourse ");

        //query.setParameter("idDepartment", idDepartment);
        query.setParameter("idCourse", id_course);
        query.setParameter("idSubject",id_subject);


        try{
            result = query.getResultList();
            return result.size();
        }catch (NoResultException e){

        }
        return result.size();

    }

    public List<ForeignStudent> fetchAllStudentStudyTheSameCourseAndSubject(int idCourse, int idSubject){
        List<ForeignStudent>listForeignStudents = null;

        Query query = entityManager.createQuery("SELECT l.foreignStudent FROM ListOfSelectedSubjects l WHERE l.subject.id=:idSubject AND l.foreignStudent.listCourses.idCourse=:idCourse ");

        query.setParameter("idCourse", idCourse);
        query.setParameter("idSubject",idSubject);

        try{
            listForeignStudents = query.getResultList();
            return listForeignStudents;
        }catch (NoResultException e){

        }
        return listForeignStudents;
    }
}
