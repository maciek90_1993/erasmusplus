package com.example.repositoryImpl;


import com.example.domain.ListDepartment;
import com.example.domain.Subjects;
import com.example.repository.ListDepartmentRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.security.auth.Subject;
import java.util.List;

/**
 * Created by Maciej on 2017-01-05.
 */
@Repository
@Transactional
public class ListDepartementRepositoryImpl implements ListDepartmentRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<ListDepartment> getAllDepartment() throws NoResultException {
        List<ListDepartment>listDepartment = null;
        Query query = entityManager.createQuery("SELECT d FROM ListDepartment d");

        try{
            listDepartment = query.getResultList();
        }catch (NoResultException e){
            listDepartment=null;
        }
        return listDepartment;
    }


}
