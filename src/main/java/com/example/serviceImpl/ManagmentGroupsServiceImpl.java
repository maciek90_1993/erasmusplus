package com.example.serviceImpl;

import com.example.domain.*;
import com.example.repository.*;
import com.example.service.ManagmentGroupsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Maciej on 2017-01-09.
 */
@Service
public class ManagmentGroupsServiceImpl implements ManagmentGroupsService {
    @Autowired
    private SubjectsRepository subjectsRepository;
    @Autowired
    private ListCoursesRepository listCoursesRepository;
    @Autowired
    private ListDepartmentRepository listDepartmentRepository;
    @Autowired
    private OffertsDepartmentRepository offertsDepartmentRepository;
    @Autowired
    private ListOfSelectedSubjectsRepository listOfSelectedSubjectsRepository;
    @Autowired
    private ListGroupsRepository listGroupsRepository;
    @Autowired
    private ForeignStudentRepository foreignStudentRepository;


    private List<Subjects> subjectsList;
    private List<ListCourses>listCourses;
    private List<ListDepartment>listDepartments;
    private List<OffertsDepartment>offertsDepartments;
    private List<Subjects>temporaryList;
    private List<ForeignStudent>foreignStudentList;
    private List<ListGroups>listGroupses;

    public Map<String, List<OffertsDepartment>> showSubjectFromCourse(){
        listCourses = listCoursesRepository.getAllCourses();
        Map<String, List<OffertsDepartment>> map = new LinkedHashMap<String, List<OffertsDepartment>>();

        for (ListCourses cours : listCourses) {
            offertsDepartments = offertsDepartmentRepository.fetchAllSubjectOnlyFromDepartmentOferts(cours.getIdCourse());

                List<OffertsDepartment> tmp = new LinkedList<OffertsDepartment>();

                tmp.addAll(offertsDepartments);


                if (offertsDepartments.size() == 0 && tmp.size() != 0) {
                    tmp.clear();
                }
                map.put(cours.getNameCourse(), tmp);

            }

        return map;
    }

    public List<ListCourses> getAllCourses(){
        listCourses = listCoursesRepository.getAllCourses();
        return listCourses;
    }


    public Map<String, Integer>amountStudentsForOneSubject(){
        listCourses = listCoursesRepository.getAllCourses();
        Map<String, Integer>amountStudents = new LinkedHashMap<String, Integer>();
        for (ListCourses cours : listCourses) {
            offertsDepartments = offertsDepartmentRepository.fetchAllSubjectOnlyFromDepartmentOferts(cours.getIdCourse());
            for (OffertsDepartment oneSubject:offertsDepartments) {
                amountStudents.put(cours.getNameCourse()+" "+oneSubject.getSubjects().getName(),listOfSelectedSubjectsRepository.getHowManyStudentsSelectedOneSubjectsFromOneCourse(cours.getIdCourse(),oneSubject.getSubjects().getId()));

            }
        }
        return amountStudents;
    }


    public void createGroups(int idCourse, int idSubject){
        List<ForeignStudent>foreignStudentList1;
        foreignStudentList1=listOfSelectedSubjectsRepository.fetchAllStudentStudyTheSameCourseAndSubject(idCourse, idSubject);
        ListCourses course = listCoursesRepository.getCourseById(idCourse);
        Subjects subject = subjectsRepository.getSubjectById(idSubject);
        listGroupsRepository.addGroups(course, subject, foreignStudentList1);
        foreignStudentList1.size();
    }

    public List<ListGroups>fetchAllGroups(){
        listGroupses = listGroupsRepository.fetchAllGroups();
        return listGroupses;
    }

    public void deleteSelectedGroup(int idGroup){
        listGroupsRepository.deleteSelectedGroupById(idGroup);
    }

    @Override
    public ListGroups findGroupById(int idGroup) {
        ListGroups listGroups=listGroupsRepository.findGroupById(idGroup);
        return listGroups;
    }

    @Override
    public void removeStudentFromGroup(int idGroup, int idStudent) {
            listGroupsRepository.updateListStudentsInGroup(idGroup,idStudent);

    }

    @Override
    public void addStudentGroup(int idGroup, String nameStudent, String surnameStudent) {
        ForeignStudent student = foreignStudentRepository.findStudentByNameAndSurname(nameStudent, surnameStudent);
        if(!student.equals(null)){
            listGroupsRepository.addStudentToGroup(idGroup, student);
        }
    }


}
