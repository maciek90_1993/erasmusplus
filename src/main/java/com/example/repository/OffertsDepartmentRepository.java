package com.example.repository;

import com.example.domain.OffertsDepartment;
import com.example.domain.Subjects;

import java.util.List;

/**
 * Created by Maciej on 2017-01-05.
 */
public interface OffertsDepartmentRepository {
    public List<OffertsDepartment>getAllOffertDepartment();
    public List<Subjects>fetchAllSubjectFromDepartmentOferts(int idCourses);
    public List<OffertsDepartment>fetchAllSubjectOnlyFromDepartmentOferts( int idCourses);
}
