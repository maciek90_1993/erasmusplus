package com.example.repository;

import com.example.domain.ListCourses;

import java.util.List;

/**
 * Created by Maciej on 2017-01-05.
 */
public interface ListCoursesRepository {

    public List<ListCourses>getAllCourses();
    public ListCourses getCourseById(int idCourse);
}
