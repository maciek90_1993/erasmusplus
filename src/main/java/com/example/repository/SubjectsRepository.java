package com.example.repository;

import com.example.domain.Subjects;

import java.util.List;


/**
 * Created by Maciej on 2017-01-04.
 */
public interface SubjectsRepository {
    public List<Subjects> getAllSubjects();
    public Subjects getSubjectById(int idSubject);
}
