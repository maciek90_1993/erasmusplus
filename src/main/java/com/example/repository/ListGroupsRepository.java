package com.example.repository;

import com.example.domain.ForeignStudent;
import com.example.domain.ListCourses;
import com.example.domain.ListGroups;
import com.example.domain.Subjects;

import java.util.List;

/**
 * Created by Maciej on 2017-01-16.
 */
public interface ListGroupsRepository {
    public void addGroups(ListCourses course, Subjects subjects, List<ForeignStudent>foreignStudentList);
    public List<ListGroups>fetchAllGroups();
    public void deleteSelectedGroupById(int idGroup);
    public ListGroups findGroupById(int idGroup);
    public void updateListStudentsInGroup(int idGroup,int idStudent);
    public void addStudentToGroup(int idGroup, ForeignStudent student);

}
