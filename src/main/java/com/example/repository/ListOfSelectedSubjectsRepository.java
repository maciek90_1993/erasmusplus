package com.example.repository;

import com.example.domain.ForeignStudent;
import com.example.domain.Subjects;

import java.util.List;

/**
 * Created by Maciej on 2017-01-12.
 */
public interface ListOfSelectedSubjectsRepository {
    List<Subjects>getAllListOfSelectedSubjectsForOneStudents(String id_Students);
    public int getHowManyStudentsSelectedOneSubjectsFromOneCourse(int id_course, int id_subject);
    public List<ForeignStudent> fetchAllStudentStudyTheSameCourseAndSubject(int idCourse, int idSubject);

}
