package com.example.repository;

import com.example.domain.ListDepartment;

import java.util.List;

/**
 * Created by Maciej on 2017-01-05.
 */
public interface ListDepartmentRepository {
    public List<ListDepartment>getAllDepartment();
}
