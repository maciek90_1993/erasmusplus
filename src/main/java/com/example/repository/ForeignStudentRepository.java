package com.example.repository;

import com.example.domain.ForeignStudent;

import java.util.List;

/**
 * Created by Piotr Łaskawski on 10.01.2017.
 */
public interface ForeignStudentRepository {
    public List<ForeignStudent> getAllForeignStudents();
    public ForeignStudent findStudentById(long idStudent);
    public ForeignStudent findStudentByNameAndSurname(String name, String surname);
    public void updataDataStudent(long idStudent, String name, String surname, String login);
}
