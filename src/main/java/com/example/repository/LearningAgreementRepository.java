package com.example.repository;

import com.example.domain.LearningAgreement;
import com.example.domain.type.StatusType;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by limack on 25.01.17.
 */
@Repository
public interface LearningAgreementRepository {

    List<LearningAgreement> getAll();
    List<LearningAgreement> getAllByStatus(StatusType status);
    void setStatusLearningAgreement(Long id, StatusType status);
    void addLearningAgreement(LearningAgreement la);
    LearningAgreement findLearningAgrementByIdForeignStudent(Long id);
    void updateListSubjectInLearninEgrement(String codesSubjects, long idLa);

}
