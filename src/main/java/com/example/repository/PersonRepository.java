package com.example.repository;

import com.example.domain.Person;
import com.example.domain.SuperUser;
import com.example.to.UserTO;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;

/**
 * Created by Maciej on 2016-12-28.
 */
@Repository
@Transactional
public class PersonRepository {

    @PersistenceContext
    private EntityManager entityManager;


    public Person getPerson(){
        Person t =entityManager.find(Person.class, 1L);
        return t ;
    }
    public Collection<Person>getAll(){
        Session session = getCurrentSession();
        return session.createCriteria(Person.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
    }

    public SuperUser findUserByLoginAndPassword(UserTO userTo) throws NoResultException{
        Query query = entityManager.createQuery("SELECT p FROM "+ userTo.getRole() + " p WHERE p.login=:login AND p.password=:password");
        query.setParameter("login", userTo.getLogin());
        query.setParameter("password", userTo.getPassword());
        SuperUser superUser = null;
        try {
            superUser = (SuperUser) query.getSingleResult();
        } catch (NoResultException e){
            return new Person();
        }
        System.out.println("person: " + superUser);
        return superUser;
    }


    private Session getCurrentSession(){
        return entityManager.unwrap(Session.class);
    }

    public void addPerson(Person newPerson){
       //entityManager.getTransaction().begin();
        //newPerson.setId(8L);
        //entityManager.merge(newPerson);
        //entityManager.getTransaction().commit();
        newPerson.setId(8L);
        getCurrentSession().merge(newPerson);
    }
}


