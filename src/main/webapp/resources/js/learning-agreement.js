$("#accept-button").click(function () {
    var idAgreement = $(this).attr('data-id-agreement');
    var notification = notificationService();
    $.post("/learning-agreement/accept", {
        idAgreement: idAgreement
    }, function (data) {
        notification.showNotification("Successful accepted learning agreement", "success");
    }).done(function () {
        location.reload();
    }).fail(function (xhr, textStatus, errorThrown) {
        notification.showNotification("Cannot accepted Learning Agreement", "danger");
    });
});

$("#decline-button").click(function () {
    var idAgreement = $(this).attr('data-id-agreement');
    var notification = notificationService();
    $.post("/learning-agreement/decline", {
        idAgreement: idAgreement
    }, function (data) {
        notification.showNotification("Successful declined learning agreement", "success");
    }).done(function () {
        location.reload();
    }).fail(function (xhr, textStatus, errorThrown) {
        notification.showNotification("Cannot declined Learning Agreement", "danger");
    });
});

$(document).on("click", "#show-detail", function () {
    var la = {
        id: $(this).data('id'),
        name: $(this).data('name'),
        lastname: $(this).data('lastname'),
        dateOfBirth: $(this).data('date-of-birth'),
        nationality: $(this).data('nationality'),
        sex: $(this).data('sex'),
        academyYear: $(this).data('academy-year'),
        langaugeCompentence: $(this).data('langauge-compentence'),
        studyCycle: $(this).data('study-cycle'),
        subjectAreaCode: $(this).data('subject-area-code'),
        phone: $(this).data('phone'),
        nameReceiving: $(this).data('name-receiving'),
        email: $(this).data('email'),
        nameSending: $(this).data('name-sending'),
        faculty: $(this).data('faculty'),
        country: $(this).data('country'),
        address: $(this).data('address'),
    }

    $("#name").text(la.name);
    $("#surname").text(la.lastname);
    $("#dateOfBirth").text(la.dateOfBirth);
    $("#nationality").text(la.nationality);
    $("#academyYear").text(la.academyYear);
    $("#langaugeCompentence").text(la.langaugeCompentence);
    $("#studyCycle").text(la.studyCycle);
    $("#subjectAreaCode").text(la.subjectAreaCode);
    $("#phone").text(la.phone);
    $("#email").text(la.email);
    $("#nameSending").text(la.nameSending);
    $("#nameReceiving").text(la.nameReceiving);
    $("#email").text(la.email);
    $("#faculty").text(la.faculty);
    $("#address").text(la.address);
    $("#sex").text(la.sex);
    $("#country").text(la.country);

    $("#accept-button").attr('data-id-agreement', la.id);
    $("#decline-button").attr('data-id-agreement', la.id);
    
});

