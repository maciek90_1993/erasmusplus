var notificationService = (function() {
    function notificationService() {
        'use strict';
        var notification = document.getElementById('notification');
        var notificationContent = document.getElementById('notification-content-id');
        
        var showNotification = function(message, className) {
            notification.style.display = "block";
            notificationContent.className = "notification-" + className;
            var msg = document.getElementById("message-notification");
            msg.textContent = message;
            setTimeout(function(){
                closeNotification();
            }, 2000);
        },
        closeNotification = function() {
           notification.style.display = "none";
        }
        return {
            closeNotification: closeNotification,
            showNotification: showNotification
        };
}
return notificationService;
})();