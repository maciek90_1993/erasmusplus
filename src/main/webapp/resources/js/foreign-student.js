//Obsługa przcisku "Dodaj La", przekierowanie do okna modalnego, z którego można dodać LA.
$(document).on("click", "#addLaButtonToModal", function () {

    var detalistStudent = {
        idStudent: $(this).data('idstudent'),
        nameStudent: $(this).data('namestudent'),
        surnameStudent: $(this).data("surnamestudent"),
        courseName: $(this).data("coursestudent")
    }

    $("#nameStudentLa").val(detalistStudent.nameStudent);
    $("#surnameStudentLa").val(detalistStudent.surnameStudent);
    $("#courseLa").val(detalistStudent.courseName);
    $("#dateOfBirthLa").val("np. April 4, 1945 - format daty");
    $("#codeSubjectsLa").val("Kody przedmiotów wprowadź po przecinku.");

});

//Obsługa przycisku "DODAJ", zczytuje dane z formularza LA i wysyła do JAVY.
$(document).on("click", "#addLA-modal-button", function(){
    var data = {
        nameStudentLa : $('#nameStudentLa').val(),
        surnameStudentLa : $('#surnameStudentLa').val(),
        adressStudentLa : $('#adressStudentLa').val(),
        academyYearLa : $('#academyYearLa').val(),
        dateOfBirthLa : $('#dateOfBirthLa').val(),
        countryLa : $('#countryLa').val(),
        codeSubjectsLa : $('#codeSubjectsLa').val(),
        departmentLa : $('#departmentLa').val(),
        languageLa : $('#languageLa').val(),
        nationalityLa : $('#nationalityLa').val(),
        phoneLa : $('#phoneLa').val(),
        sexLa : $('#sexLa').val()
    }

    var notification = notificationService();
    $.post("/addLA", {
        nameStudent : data.nameStudentLa,
        surnameStudent: data.surnameStudentLa,
        adressStudent: data.adressStudentLa,
        academyYear: data.academyYearLa,
        dateBirth:data.dateOfBirthLa,
        country:data.countryLa,
        codeSubjects:data.codeSubjectsLa,
        department:data.departmentLa,
        language:data.languageLa,
        nationality:data.nationalityLa,
        phone:data.phoneLa,
        sex:data.sexLa
    }, function (data) {
        location.reload();
    }).done(function () {
        notification.showNotification("Pomylśnie dodano LA", "success");
    }).fail(function (xhr, textStatus, errorThrown) {
        notification.showNotification("Problem z dodaniem LA", "danger");
    });
});

//Funkcja sprawdza czy dany użytkownik dodał już la. Jeśli tak to blokowany jest przycisk "Dodaj LA".
function checkThatUserHaveAddedLa(){
    var notification = notificationService();
    $.ajax({
        type     : "GET",
        url      : "/checkCreateLA",
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function(json) {
            if(json != null){
                console.log("Suksces sprawdzania czu user ma LA !");
                $("#addLaButtonToModal").attr("disabled", true);
            }else{
                $("#addLaButtonToModal").attr("disabled", false);
            }
        },
        complete: function() {
            //ten fragment wykona się po zakończeniu łączenia - nie ważne czy wystąpił błąd, czy sukces
        },
        error: function(jqXHR, errorText, errorThrown) {
            notification.showNotification("Problem z odnalezieniem LA dla danego studenta !", "danger");
        }
    });
}

//Wyświetla LA użytkownika
$(document).on("click", "#showUserLa", function(){
    var notification = notificationService();
    $.ajax({
        type     : "GET",
        url      : "/checkCreateLA",
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function(json) {
            if(json != null){
                $.each(json, function(i, ob){
                    console.log(i, ob);
                    if(i=='name')$('#nameSla').text(ob);

                    if(i == "lastname")$('#surnameSla').text(ob);
                    if(i == 'dateOfBirth')$('#dateOfBirthSla').text(ob);
                    if(i== 'nationality')$('#nationalitySla').text(ob);
                    if(i == 'sex')$('#sexSla').text(ob);
                    if(i == 'academyYear')$('#academyYearSla').text(ob);
                    if(i == 'langaugeCompentence')$('#langaugeCompentenceSla').text(ob);
                    if(i == 'studyCycle')$('#studyCycleSla').text(ob);
                    if(i == 'subjectAreaCode')$('#subjectAreaCodeSla').text(ob);
                    if(i == 'phone')$('#phoneSla').text(ob);
                    if(i == 'email')$('#emailSla').text(ob);
                    if(i == 'departnent')$('#facultySla').text(ob);
                    if(i == 'country')$('#countrySla').text(ob);
                });
            }
        },
        complete: function() {
            //ten fragment wykona się po zakończeniu łączenia - nie ważne czy wystąpił błąd, czy sukces
        },
        error: function(jqXHR, errorText, errorThrown) {
            notification.showNotification("Problem z odnalezieniem LA dla danego studenta przycisk wyświetl LA !", "danger");
        }
    });
});


//Obsługa przycisku "Edytuj liste przedmiotów.
$(document).on("click", "#change-list-subject-la", function(){
    window.location.replace("/showListSubject");
});


//Obsługa przycisku, który zmienia kody przedmiotów w LA użytkownika.
$(document).on("click", "#change-list-subject-la-student", function(){
    var subject1 = $("#subject1").val();
    var subject2 = $("#subject2").val();
    var subject3 = $("#subject3").val();
    var notification = notificationService();
    $.post("/updateLaCodeSubjects", {
       subject1:subject1,
        subject2:subject2,
        subject3:subject3
    }, function (data) {
        location.reload();
    }).done(function () {
        notification.showNotification("Pomylśnie zmieniono przedmioty w LA", "success");
    }).fail(function (xhr, textStatus, errorThrown) {
        notification.showNotification("Problem ze zmianą przedmiotów w LA", "danger");
    });
});

//Obsługa przycisku do edycji danych studenta
$(document).on("click", "#save-new-data-user", function(){
    var nameStudent = $('#nameUser').val();
    var surnameStudent = $('#surnameUser').val();
    var loginStudent = $('#loginUser').val();
    var notification = notificationService();

    $.post("/updataDataUser", {
        nameStudent:nameStudent,
        surnameStudent:surnameStudent,
        loginStudent:loginStudent
    }, function (data) {
        location.reload();
    }).done(function () {
        notification.showNotification("Pomylśnie zmieniono Twoje dane personalne", "success");
    }).fail(function (xhr, textStatus, errorThrown) {
        notification.showNotification("Problem ze zmianą Twoich danych personalnych", "danger");
    });

});