/*
$( ".btn-success" ).click(  function( event ) {
    var idCourseValue = $('.btn-success').attr('data-idcourse');
    var idSubjectValue = $('.btn-success').attr('data-idSubject');
    console.log(idCourseValue + idSubjectValue);
});*/

//Obłusga przycisku tworzenia grupy gdy jest napis "Dodaj"
$( "table" ).delegate( "button", "click", function() {
    var idCourseValue = $( this ).attr('data-idcourse');
    var idSubjectValue = $(this).attr('data-idsubject');
    var tmp = $(this);
    var notification = notificationService();
    console.log(idCourseValue + idSubjectValue);
    if(!tmp.hasClass("btn-danger")) {
        $.post("/Groups/createGroup", {
            idCourse: idCourseValue,
            idSubject: idSubjectValue
        }, function (data) {
           // alert("Grupa została stworzona !" + $('#info').text());
            location.reload();
        }).done(function () {
            tmp.addClass("btn-primary");
            tmp.removeClass("btn-success");
            tmp.html("Utworzona");
            tmp.attr("disabled", true);
            notification.showNotification("Grupa została poprawnie stworzona", "success");
        }).fail(function (xhr, textStatus, errorThrown) {
            notification.showNotification('Grupa nie została stworzona!', "danger");
        });
    }
});

//Obsługa przycisku usuwania grupy.
$("span").delegate("button", "click", function(){

    var idGroup = $(this).attr('data-idgroup');
    if(idGroup != null){
        console.log(idGroup);
        var tmp = $(this);
        var notification = notificationService();
    if(!tmp.hasClass("btn-danger")) {


        $.post("/Groups/deleteGroup", {
            idGroup: idGroup
        }, function (data) {
            location.reload();

        }).done(function () {
            notification.showNotification("Grupa została poprawnie usunięta", "success");
        }).fail(function (xhr, textStatus, errorThrown) {

            notification.showNotification('Grupa nie została usunięta!', "danger");
        });
    }
    }


});

/* Funkcja zwraca się ajax do kontrolera ManagmentGroups aby ten zwrócił jakie grupy są utworzone.
* Następnie w bloku success pobierane są wszytkie atrybuty przycisków, które posiadają klase btn-success
 * i blokowana jest opcja ponownego utworzenia tej samej grupy na dany przedmiot na danym kierunku.
* */
function checkCreatedGroups(){
    $.ajax({
        type     : "GET",
        url      : "/Groups/checkWhichGroupsWasCreate",
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function(json) {
            console.log("Dane zwrócono z controlera");

            $.each(json, function(i, ob){
                $(".btn-success").each(function(index, value){

                    if($(this).attr("data-idcourse")==ob.course.idCourse && $(this).attr("data-idsubject") == ob.subject.id ){
                        $(this).addClass("btn-primary");
                        $(this).removeClass("btn-success");
                        $(this).html("Utworzona");
                        $(this).attr("disabled", true);
                    }
                });
            });
        },
        complete: function() {
            //ten fragment wykona się po zakończeniu łączenia - nie ważne czy wystąpił błąd, czy sukces
        },
        error: function(jqXHR, errorText, errorThrown) {
            console.log("Błąd: "+errorText + " " +errorThrown);
        }
    });
}
//Obłsuga przycisku, który usuwa studenta z grupy

$( "#tableGroups" ).on( "click", "button ", function( event ) {
    var button = event.target;
    var notification = notificationService();
    var idGroup = $(this).attr('data-idgroup');
    var idStudent = $(this).attr('data-idstudent');
    console.log( $( this ).text() );

    $.post("/Groups/deleteStudentFromGroup", {
        idGroup: idGroup,
        idStudent: idStudent
    }, function (data) {
        location.reload();
        notification.showNotification("Student został usunięty", "success");
    }).done(function () {
        //$(document).delay(5000);
      //  location.reload();
    }).fail(function (xhr, textStatus, errorThrown) {
        notification.showNotification("Student nie został usunięty", "danger");
    });



});

// Wyświetla okono modalne z formularzem, za pomocą którego można dodać studenta do grupy.
$(document).on("click", "#addStudentToGroup", function (event) {
    var button = event.target;
    var nameCourse = $(this).attr('data-namecourse');
    var nameSubject=$(this).attr('data-namesubject');
    var idGroup = $(this).attr('data-idgroup');
    $('#infoCourseSubject').text(nameCourse + " "+ nameSubject);

    $("#add-modal-button").attr('data-idgroup',idGroup);
});


//Przycisk dodaje  studenta do grupy.
$('#add-modal-button').click(function (){
    var idGroup=$(this).attr('data-idgroup');
    var nameStudent = $('#nameStudent').val();
    var surnameStudent = $('#surnameStudent').val();
    var notification = notificationService();
    var data={
        name:nameStudent,
        surname:surnameStudent
    }
    $.get("/Groups/addToGroup", {
        idGroup: idGroup,
        nameStudent:nameStudent,
        surnameStudent:surnameStudent
    }, function (data) {
        location.reload();
        notification.showNotification('Student została dodany poprawnie', "success");
    }).done(function () {

    }).fail(function (xhr, textStatus, errorThrown) {
        notification.showNotification('Student nie została dodany poprawnie', "danger");
    });
});

