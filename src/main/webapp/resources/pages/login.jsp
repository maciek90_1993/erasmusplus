<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>



<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>

<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Logowanie</h3>
				</div>
				<div class="panel-body">
					<c:if test="${not empty error}">
						<div class="error">${error}</div>
					</c:if>
					<c:if test="${not empty msg}">
						<div class="msg">${msg}</div>
					</c:if>

					 <form:form method="post" action="j_spring_security_check"
						modelAttribute="users">
						<fieldset>
							<div class="form-group">
									<form:input path="username" class="form-control" placeholder="Id"/>
							</div>
							<div class="form-group">
									<form:input path="password" type="password" class="form-control" placeholder="Haslo"/>
							</div>
							<!-- Change this to a button or input when using this as a form -->
							<input name="submit" type="submit" value="Zaloguj" class="btn btn-lg btn-success btn-block"/>
							
							
						</fieldset>
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
					</form:form>


				</div>
			</div>
		</div>
	</div>
</div>

