<%--
  Created by IntelliJ IDEA.
  User: Maciej
  Date: 2017-01-12
  Time: 17:21
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Piotr Łaskawski
  Date: 10.01.2017
  Time: 12:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Zarządzanie studentem zagranicznym</title>

    <!-- Bootstrap Core CSS -->
    <link href="../resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../resources/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../resources/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../resources/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../resources/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../resources/css/notification.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <jsp:include page="template_static/header.template.jsp" />
        <jsp:include page="template_static/left-nav.template.jsp" />
    </nav>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Student zagraniczny</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class=" col-md-9 col-lg-9 ">

                <table class="table table-user-information">
                    <tbody>
                    <c:if test="${not empty currentStudent}">
                        <tr>
                            <td>Imię:</td>
                            <td>${currentStudent.firstName}</td>
                        </tr>
                        <tr>
                            <td>Nazwisko:</td>
                            <td>${currentStudent.lastName}</td>
                        </tr>
                        <tr>
                            <td>Login</td>
                            <td>${currentStudent.login}</td>
                        </tr>

                        <tr>
                        </tr><tr>
                            <td>Kierunek</td>
                            <td>${currentStudent.listCourses.nameCourse}</td>
                        </tr>

                    </c:if>
                    </tbody>
                </table>

                <a class="btn btn-success"
                   id="addLaButtonToModal"
                   data-idstudent ="${currentStudent.id}"
                   data-namestudent="${currentStudent.firstName}"
                   data-surnamestudent="${currentStudent.lastName}"
                   data-coursestudent="${currentStudent.listCourses.nameCourse}"
                   data-toggle="modal" href="#addLAModal">
                    Dodaj LA</a>
                <a  id="showUserLa" class="btn btn-primary" data-toggle="modal" href="#showLaModal">Wyświetl LA</a>
                <a  class="btn btn-primary" data-toggle="modal" href="#edit-user-personal-data">Edytuj swoje dane</a>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-6">

                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->

        <!-- /.row -->

        <!-- /.row -->

        <!-- /.col-lg-6 -->

        <!-- /.col-lg-6 -->
    </div>

    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
</div>
<div id="notification" class="notification-modal">
    <div id="notification-content-id">
        <span class="close">&times;</span>
        <span id="message-notification"></span>
    </div>
</div>
</div>


</div>
<!--[START] ADD LA MODAL -->
<div class="modal fade" id="addLAModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dodaj Learing Agreement</h4>
            </div>
            <div class="modal-body">

                    <p><span class="info-left" style="font-weight: bold;">Imię: </span><span><input  type="text" class="form-control" id="nameStudentLa" /></span> </p>
                    <p><span class="info-left" style="font-weight: bold;">Nazwisko: </span> <span ><input  class="form-control" type="text" id="surnameStudentLa" /></span> </p>
                    <p><span class="info-left" style="font-weight: bold;">Adres: </span><span><input  type="text" class="form-control" id="adressStudentLa"/></span> </p>
                    <p><span class="info-left" style="font-weight: bold;">Rok akademicki: </span><span><input  type="text" class="form-control" id="academyYearLa"/></span> </p>
                    <p><span class="info-left" style="font-weight: bold;">Data urodzenia: </span><span><input  type="text" class="form-control" id="dateOfBirthLa"/></span> </p>
                    <p><span class="info-left" style="font-weight: bold;">Kraj: </span><span><input path="country" type="text" class="form-control" id="countryLa"/></span> </p>
                    <p><span class="info-left" style="font-weight: bold;">Kierunek: </span><span><input path="faculty" type="text" class="form-control" id="courseLa"/></span> </p>
                    <p><span class="info-left" style="font-weight: bold;">Kody wybranych przedmiotów: </span><span><input type="text" class="form-control" id="codeSubjectsLa"/></span> </p>
                    <p><span class="info-left" style="font-weight: bold;">Wydział: </span><span><input type="text" class="form-control" id="departmentLa"/></span> </p>
                    <p><span class="info-left" style="font-weight: bold;">Język: </span><span><input type="text" class="form-control" id="languageLa"/></span> </p>
                    <p><span class="info-left" style="font-weight: bold;">Narodowość: </span><span><input type="text" class="form-control" id="nationalityLa"/></span> </p>
                    <p><span class="info-left" style="font-weight: bold;">Telefon: </span><span><input type="text" class="form-control" id="phoneLa"/></span> </p>
                    <p><span class="info-left" style="font-weight: bold;">Płeć: </span><span><input type="text" class="form-control" id="sexLa"/></span> </p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="addLA-modal-button" data-dismiss="modal" >Dodaj</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
            </div>
        </div>
    </div>
</div>
<!--[END] ADD LA MODAL-->
<!--{START] SHOW LA MODAL -->
<div class="modal fade" id="showLaModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Learning Agreement</h4>
            </div>
            <div class="modal-body">

                <p><span class="info-left" style="font-weight: bold;">Imię: </span> <span id="nameSla"></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Nazwisko: </span> <span id="surnameSla"></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Data urodzenia: </span> <span id="dateOfBirthSla"></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Narodowość: </span> <span id="nationalitySla"></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Płeć: </span> <span id="sexSla"></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Rok akademicki: </span> <span id="academyYearSla"></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Język korespondencji: </span> <span id="langaugeCompentenceSla"></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Cykl studiów: </span> <span id="studyCycleSla"></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Wybrane przedmioty: </span> <span id="subjectAreaCodeSla"></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Telefon: </span> <span id="phoneSla"></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Email: </span> <span id="emailSla"></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Wydział: </span> <span id="facultySla"></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Kraj: </span> <span id="countrySla"></span> </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" id="change-list-subject-la" data-dismiss="modal" >Edytuj liste przedmiotów</button>

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--[END] SHOW LA MODAL-->
<!--[START] EDIT PERSONALITY USER MODAL -->
<div class="modal fade" id="edit-user-personal-data">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edytuj swoje dane</h4>
            </div>
            <div class="modal-body">
                <p><span class="info-left" style="font-weight: bold;">Imię: </span><span><input  type="text" class="form-control" id="nameUser" placeholder="Może być puste" /></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Nazwisko: </span><span><input  type="text" class="form-control" id="surnameUser" placeholder="Może być puste"/></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Login: </span><span><input  type="text" class="form-control" id="loginUser" placeholder="Może być puste"/></span> </p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" id="save-new-data-user" data-dismiss="modal" >Zapisz zmiany</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--[END] EDIT PERSONALITY USER MODAL -->
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../resources/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../resources/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../resources/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="../resources/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="../resources/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="../resources/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../resources/dist/js/sb-admin-2.js"></script>
<script src="../resources/js/foreign-student.js"></script>
<script type="text/javascript" src="../resources/js/notification.service.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
        checkThatUserHaveAddedLa();
    });
</script>

</body>
</html>
