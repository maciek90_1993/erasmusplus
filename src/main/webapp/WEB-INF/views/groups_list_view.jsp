<%--suppress ALL --%>
<%--
  Created by IntelliJ IDEA.
  User: Maciej
  Date: 2017-01-16
  Time: 17:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Zarządzanie grupami i przedmiotami</title>

    <!-- Bootstrap Core CSS -->
    <link href="../resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../resources/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../resources/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../resources/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../resources/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="../resources/css/notification.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<%--suppress JSUnresolvedFunction --%>

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <jsp:include page="template_static/header.template.jsp" />
        <jsp:include page="template_static/left-nav.template.jsp" />
    </nav>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Lista utworzonych grup.</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.panel -->
        <span>


        <c:forEach items="${listGroups}" var="group">
        <div class="panel panel-default">
            <div  class="panel-heading">
                ${group.course.nameCourse} - ${group.subject.name} <button type="button" class="btn btn-lg btn-warning" data-idgroup="${group.idGroup}" style="margin-left: 530px;">Usuń grupe</button>

            </div>
            <div id="addStudentToGroup" class="btn btn-primary" href="#server_modal_add_student_to_group"
                data-toggle="modal" data-idcourse="${group.course.idCourse}" data-idsubject="${group.subject.id} " data-namecourse="${group.course.nameCourse}" data-namesubject="${group.subject.name}" data-idgroup="${group.idGroup}">Dodaj studenta</div>

            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="tableGroups" class="table">

                        <p id="info"></p>
                        <c:choose>
                        <c:when test="${not empty group.foreignStudentList}">
                        <thead>
                        <tr>
                            <th>Imię</th>
                            <th>Nazwisko</th>
                            <th>Akcja</th>
                        </tr>
                        </thead>
                            <tbody>
                            <c:forEach items="${group.foreignStudentList}" var="student" varStatus="theCount">


                                     <tr class="success">
                                         <td>${student.firstName}</td>
                                         <td>${student.lastName}</td>
                                         <td><button class="btn-danger" data-dismiss="modal" data-idstudent="${student.id}" data-idgroup="${group.idGroup}">Usuń</button></td>
                                     </tr>

                        </c:forEach>
                            </tbody>
                        </c:when>
                        <c:when test="${empty group.foreignStudentList}">
                            <thead>
                            </thead>
                            <tbody>
                                    <tr class="alert alert-danger">

                                        <td>Brak studentów w tej grupie</td>
                                    </tr>
                            </tbody>
                        </c:when>
                        </c:choose>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div><br />
            <!-- /.panel-body -->
        </div>
        </c:forEach>
            </span>
        <!-- /.row -->



    </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-6">

                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->

        <!-- /.row -->

        <!-- /.row -->

        <!-- /.col-lg-6 -->

        <!-- /.col-lg-6 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
    <div id="notification" class="notification-modal">
        <div id="notification-content-id">
            <span class="close">&times;</span>
            <span id="message-notification"></span>
        </div>
</div>
</div>
<!--[START MODAL] ADD FOREIGNSTUDENT TO GROUP -->
<div class="modal fade" id="server_modal_add_student_to_group">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Dodaj studenta do grupy</h4>
            </div>
            <div class="modal-body">
                <p><span class="info-left" style="font-weight: bold;">Imię: </span><span><input type="text" class="form-control" id="nameStudent" value="Podaj imię studenta"/></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Nazwisko: </span> <span ><input class="form-control" type="text" id="surnameStudent" value="Podaj nazwisko studenta"/></span> </p>
                <p><span class="info-left" style="font-weight: bold;">Dodajesz studenta do grupy na:</span><span  id="infoCourseSubject" ></span></p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="add-modal-button" data-dismiss="modal" >Dodaj</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Anuluj</button>
            </div>
        </div>
    </div>
</div>
<!--[END] ADD FOREIGNSTUDENT TO GROUP -->

<!-- /#wrapper -->

<!-- jQuery -->
<script src="../resources/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../resources/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../resources/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="../resources/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="../resources/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="../resources/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../resources/dist/js/sb-admin-2.js"></script>
<script type="text/javascript" src="../resources/js/managmentGroups.js"></script>
<script type="text/javascript" src="../resources/js/learning-agreement.js"></script>
<script type="text/javascript" src="../resources/js/notification.service.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });

    });



</script>

</body>

</html>