<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${currentUser.role == 'ForeignStudent'}">
   <jsp:include page="navs-role/foreign-student.template.jsp" />
</c:if>

<c:if test="${currentUser.role == 'Person'}">
   <jsp:include page="navs-role/employee-university.template.jsp" />
</c:if>
