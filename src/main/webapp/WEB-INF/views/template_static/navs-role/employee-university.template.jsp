<meta charset="utf-8">
<%@ page contentType="text/html; charset=UTF-8" %>
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>

            <li>
                <a href="#"><i class="fa fa-sitemap fa-fw"></i>Zarządzanie grupami<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="/learning-agreement">Lerning Agreement</a>
                    </li>
                    <li>
                        <a id="ListGrops" href="/Groups/listGroups">Lista grup</a>
                    </li>
                    <li>
                        <a id="ListCourse" href="/Groups">Lista kierunków i przedmiotów </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>