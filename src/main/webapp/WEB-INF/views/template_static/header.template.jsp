<ul class="nav navbar-top-links navbar-left">
    <a class="navbar-brand" href="/">Erasmus++</a>
</ul>

<ul class="nav navbar-top-links navbar-right">
    <a class="navbar-brand" href="/">Logged as ${currentUser.login} <span class="role">( ${currentUser.role})</span></a>
    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                   <li><a href="http://localhost:8080/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
        </li>
    </ul>
</ul>