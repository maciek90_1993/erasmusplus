<%--
  Created by IntelliJ IDEA.
  User: Maciej
  Date: 2017-02-08
  Time: 18:09
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Zarządzanie studentem zagranicznym</title>

    <!-- Bootstrap Core CSS -->
    <link href="../resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../resources/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../resources/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../resources/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../resources/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../resources/css/notification.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <jsp:include page="template_static/header.template.jsp" />
        <jsp:include page="template_static/left-nav.template.jsp" />
    </nav>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Lista dostępnych przedmiotów na twoim kierunku</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                       Lista przedmiotów
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>Kod</th>
                                <th>Nazwa</th>
                                <th>Semestr</th>
                                <th>Wydział</th>

                            </tr>

                            </thead>
                            <tbody>
                                <tr>
                                    <th></th>
                                </tr>
                                    <c:if test="${not empty subjectList}">
                                        <c:forEach items="${subjectList}" var="data">
                                            <tr class="odd gradeX">
                                                <td>${data.subjects.cod}</td>
                                                <td>${data.subjects.name}</td>
                                                <td>${data.subjects.term}</td>
                                                <td>${data.department.nameDepartment}</td>

                                            </tr>
                                    </c:forEach>
                                </c:if>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->

                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <h1 class="page-header">Wypełnij formularz jeśli chcesz zmienić przedmioty w LA</h1>
            </div>
            <div class="col-lg-6">
                <label>Kod przedmiotu nr 1</label>
                <input id="subject1" placeholder="Pole może być puste" class="form-control">
                <label>Kod przedmiotu nr 2</label>
                <input id="subject2" class="form-control" placeholder="Pole może być puste">
                <label>Kod przedmiotu nr 3</label>
                <input id="subject3"class="form-control" placeholder="Pole może być puste"><br />
                <button type="button" class="btn btn-success" id="change-list-subject-la-student">Zmień przedmioty</button>
                </div>
            </div>
    <!-- /.row -->
    </div>
<!-- /#page-wrapper -->
</div>
<div id="notification" class="notification-modal">
    <div id="notification-content-id">
        <span class="close">&times;</span>
        <span id="message-notification"></span>
    </div>
</div>




<!--[START] ADD LA MODAL -->

</div>
<!--[END] SHOW LA MODAL-->

<!-- /#wrapper -->

<!-- jQuery -->
<script src="../resources/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../resources/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="../resources/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="../resources/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="../resources/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="../resources/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../resources/dist/js/sb-admin-2.js"></script>
<script src="../resources/js/foreign-student.js"></script>
<script type="text/javascript" src="../resources/js/notification.service.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });

    });
</script>

</body>
</html>
