<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
            <html lang="en">
            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="description" content="">
                <meta name="author" content="">
                <title>SB Admin 2 - Bootstrap Admin Theme</title>
                <link href="../resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
                <link href="../resources/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
                <link href="../resources/dist/css/sb-admin-2.css" rel="stylesheet">
                <link href="../resources/vendor/morrisjs/morris.css" rel="stylesheet">
                <link href="../resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
            </head>

            <body>
                <div id="wrapper">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="login-panel panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Sign in</h3>
                                </div>
                                <div class="panel-body">
                                    <form:form modelAttribute="loginPerson" method="post" action="/login">
                                        <form:input path="login" id="login" type="text" class="form-control" placeholder="Login" required="required" /><br />
                                        <form:input path="password" type="password" id="password" class="form-control" placeholder="Password" required="required"
                                        /><br />
                                        <div class="form-group">
                                            <label>Loggin as</label>
                                            <form:select path="role" class="form-control">
                                                <option value="ForeignStudent">Foreign Student</option>
                                                <option value="Person">Person</option>
                                                <option>Coordinator -- Unavailable --</option>
                                                <option>CIS Employee -- Unavailable --</option>
                                                <option>University Employee -- Unavailable --</option>
                                            </form:select>
                                        </div>
                                        <input class="btn btn-lg btn-success btn-block" type="submit" id="addButton" value="Login" />
                                    </form:form>
                                    <c:if test="${not empty message}">
                                        <div class="alert alert-danger" style="margin-top: 10px;"> ${message}</div>
                                    </c:if>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
        <script src="../resources/vendor/jquery/jquery.min.js"></script>
        <script src="../resources/vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="../resources/vendor/metisMenu/metisMenu.min.js"></script>
        <script src="../resources/data/morris-data.js"></script>
        <script src="../resources/dist/js/sb-admin-2.js"></script>
        <script type="text/javascript" src="../resources/js/managmentGroups.js"></script>
    </body>

</html>