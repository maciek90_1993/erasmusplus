<%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
            <!DOCTYPE html>
            <html lang="en">

            <head>

                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="description" content="">
                <meta name="author" content="">
                <title>Learning Agreement</title>

                <link href="../resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
                <link href="../resources/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
                <link href="../resources/dist/css/sb-admin-2.css" rel="stylesheet">
                <link href="../resources/vendor/morrisjs/morris.css" rel="stylesheet">
                <link href="../resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
                <link href="../resources/css/notification.css" rel="stylesheet">
                <style>
                .info-left {
                    font-weight: bold;
                }
    </style>

</head>

<body>
    <div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <jsp:include page="template_static/header.template.jsp" />
        <jsp:include page="template_static/left-nav.template.jsp" />
    </nav>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Lerning agreement</h1>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">For consideration</h3>
        </div>
    </div>
        <c:if test="${empty learningAgreementsToCheck}">
        <div style="padding: 15px;">
            <center>No learining agreement to consideration</center>
        </div>
    </c:if>
    <c:if test="${not empty learningAgreementsToCheck}">
        <div class="row">
            <div class="panel-body">
                <table width="100%" class="table table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Erasmus code</th>
                            <th>Fullname</th>
                            <th>Nationality</th>
                            <th>Academy year</th>
                            <th>Country</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${learningAgreementsToCheck}" var="la">
                            <tr>
                                <td>${la.id}</td>
                                <td>${la.name} ${la.lastname}</td>
                                <td>${la.nationality}</td>
                                <td>${la.academyYear}</td>
                                <td>${la.country}</td>
                                <td>
                                    <div class="btn btn-primary"
                                        id="show-detail" 
                                        href="#server_msg_modal"   
                                        data-toggle="modal"
                                        data-id="${la.id}"
                                        data-name="${la.name}"
                                        data-lastname="${la.lastname}"
                                        data-date-of-birth="${la.dateOfBirth}"
                                        data-nationality="${la.nationality}"
                                        data-sex="${la.sex}"
                                        data-academy-year="${la.academyYear}"
                                        data-langauge-compentence="${la.langaugeCompentence}"
                                        data-study-cycle="${la.studyCycle}"
                                        data-subject-area-code="${la.subjectAreaCode}"
                                        data-phone="${la.phone}"
                                        data-email="${la.email}"
                                        data-name-sending="${la.nameSending}"
                                        data-name-receiving="${la.nameReceiving}"
                                        data-faculty="${la.faculty}"
                                        data-country="${la.country}"
                                        data-address="${la.address}">View</div>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </c:if>

    <!--[START]ACCEPTED LEARNING AGREEMENT TABLE-->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Accepted</h3>
        </div>
    </div>
    <c:if test="${empty learningAgreementsAccepted}">
        <div style="padding: 15px;">
            <center>No accepted learining agreement</center>
        </div>
    </c:if>
    <c:if test="${not empty learningAgreementsAccepted}">
        <div class="row">
            <div class="panel-body">
                <table width="100%" class="table table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Erasmus code</th>
                            <th>Fullname</th>
                            <th>Nationality</th>
                            <th>Academy year</th>
                            <th>Country</th>
                            <th></th>
                        </tr>

                    </thead>
                    <tbody>
                        <c:forEach items="${learningAgreementsAccepted}" var="la">
                            <tr>
                                <td>${la.id}</td>
                                <td>${la.name} ${la.lastname}</td>
                                <td>${la.nationality}</td>
                                <td>${la.academyYear}</td>
                                <td>${la.country}</td>
                                <td>
                                    <div class="btn btn-primary"
                                        id="show-detail" 
                                        href="#server_msg_modal"   
                                        data-toggle="modal"
                                        data-id="${la.id}"
                                        data-name="${la.name}"
                                        data-lastname="${la.lastname}"
                                        data-date-of-birth="${la.dateOfBirth}"
                                        data-nationality="${la.nationality}"
                                        data-sex="${la.sex}"
                                        data-academy-year="${la.academyYear}"
                                        data-langauge-compentence="${la.langaugeCompentence}"
                                        data-study-cycle="${la.studyCycle}"
                                        data-subject-area-code="${la.subjectAreaCode}"
                                        data-phone="${la.phone}"
                                        data-email="${la.email}"
                                        data-name-sending="${la.nameSending}"
                                        data-name-receiving="${la.nameReceiving}"
                                        data-faculty="${la.faculty}"
                                        data-country="${la.country}"
                                        data-address="${la.address}">View</div>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </c:if>
    <!--[END] ACCEPTED LEARNING AGREEMENT TABLE-->


    <!--[START] DECLINED LEARNING AGREEMENT TABLE-->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Declined</h3>
        </div>
    </div>
    <c:if test="${empty learningAgreementsDeclined}">
        <div style="padding: 15px;">
            <center>No declined learining agreement</center>
        </div>
    </c:if>
    <c:if test="${not empty learningAgreementsDeclined}">
        <div class="row">
            <div class="panel-body">
                <table width="100%" class="table table-hover" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Erasmus code</th>
                            <th>Fullname</th>
                            <th>Nationality</th>
                            <th>Academy year</th>
                            <th>Country</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${learningAgreementsDeclined}" var="la">
                            <tr>
                                <td>${la.id}</td>
                                <td>${la.name} ${la.lastname}</td>
                                <td>${la.nationality}</td>
                                <td>${la.academyYear}</td>
                                <td>${la.country}</td>
                                <td>
                                    <div class="btn btn-primary"
                                        id="show-detail" 
                                        href="#server_msg_modal"   
                                        data-toggle="modal"
                                        data-id="${la.id}"
                                        data-name="${la.name}"
                                        data-lastname="${la.lastname}"
                                        data-date-of-birth="${la.dateOfBirth}"
                                        data-nationality="${la.nationality}"
                                        data-sex="${la.sex}"
                                        data-academy-year="${la.academyYear}"
                                        data-langauge-compentence="${la.langaugeCompentence}"
                                        data-study-cycle="${la.studyCycle}"
                                        data-subject-area-code="${la.subjectAreaCode}"
                                        data-phone="${la.phone}"
                                        data-email="${la.email}"
                                        data-name-sending="${la.nameSending}"
                                        data-name-receiving="${la.nameReceiving}"
                                        data-faculty="${la.faculty}"
                                        data-country="${la.country}"
                                        data-address="${la.address}">View</div>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </c:if>
    <!--[END] DECLINED LEARNING AGREEMENT TABLE-->


</div>
    <div id="notification" class="notification-modal">
        <div id="notification-content-id">
            <span class="close">&times;</span>
            <span id="message-notification"></span>
        </div>
    </div>
</div>

<!--[START] DETAILS LEARNING AGREEMENT MODAL-->
<div class="modal fade" id="server_msg_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Learning Agreement</h4>
            </div>
            <div class="modal-body">
                <p><span class="info-left">Name: </span> <span id="name"></span> </p>  
                <p><span class="info-left">Surname: </span> <span id="surname"></span> </p>
                <p><span class="info-left">Date of birth: </span> <span id="dateOfBirth"></span> </p>
                <p><span class="info-left">Nationality: </span> <span id="nationality"></span> </p>
                <p><span class="info-left">Sex: </span> <span id="sex"></span> </p>
                <p><span class="info-left">Academy year: </span> <span id="academyYear"></span> </p>
                <p><span class="info-left">Langauge compentence: </span> <span id="langaugeCompentence"></span> </p>
                <p><span class="info-left">Study sycle: </span> <span id="studyCycle"></span> </p>
                <p><span class="info-left">Subject area code: </span> <span id="subjectAreaCode"></span> </p>
                <p><span class="info-left">Phone: </span> <span id="phone"></span> </p>
                <p><span class="info-left">Email: </span> <span id="email"></span> </p>
                <p><span class="info-left">Name sending: </span> <span id="nameSending"></span> </p>
                <p><span class="info-left">Person contact name: </span> <span id="nameReceiving"xx</span> </p>
                <p><span class="info-left">Faculty: </span> <span id="faculty"></span> </p>
                <p><span class="info-left">Country: </span> <span id="country"></span> </p>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-success" id="accept-button" data-dismiss="modal" >Accept</button>
           <button type="button" class="btn btn-danger" id="decline-button" data-dismiss="modal">Decline</button> 
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!--[END] DETAILS LEARNING AGREEMENT MODAL-->

<script src="../resources/vendor/jquery/jquery.min.js"></script>
<script src="../resources/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="../resources/vendor/metisMenu/metisMenu.min.js"></script>
<script src="../resources/dist/js/sb-admin-2.js"></script>
<script type="text/javascript" src="../resources/js/managmentGroups.js"></script>
<script type="text/javascript" src="../resources/js/learning-agreement.js"></script>
<script type="text/javascript" src="../resources/js/notification.service.js"></script>
<script>
</script>
</body>

</html>