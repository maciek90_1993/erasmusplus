<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Zarządzanie grupami i przedmiotami</title>

    <!-- Bootstrap Core CSS -->
    <link href="../resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../resources/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../resources/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../resources/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../resources/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../resources/css/notification.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <jsp:include page="template_static/header.template.jsp" />
            <jsp:include page="template_static/left-nav.template.jsp" />
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Przedmioty na danym kierunku studiów</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            DataTables Advanced Tables
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Kod</th>
                                        <th>Nazwa</th>
                                        <th>Semestr</th>
                                        <th>Wydział</th>
                                        <th>Liczba chętnych</th>
                                        <th>Akcja</th>
                                    </tr>

                                </thead>
                                <tbody>

                                    <c:set var="buttonEnable" value="disabled"/>

                                    <c:forEach items="${subjects}"  var="key">
                                        <tr>
                                            <th>${key.key}</th>
                                        </tr>
                                                <c:forEach items="${key.value}" var="type">
                                            <tr class="odd gradeX">
                                                    <td>${type.subjects.cod}</td>
                                                    <td>${type.subjects.name}</td>
                                                    <td>${type.subjects.term}</td>
                                                    <td>${type.department.nameDepartment}</td>
                                                    <c:forEach items="${amountsStudent}" var="amountStudent">
                                                        <c:set var="condition" value="${key.key} ${type.subjects.name}"/>
                                                        <c:if test="${condition == amountStudent.key}">
                                                            <td>${amountStudent.value}</td>
                                                            <c:choose>
                                                                <c:when test="${amountStudent.value <5 }">
                                                                    <td><button  type="button" ${buttonEnable}  class = "btn btn-lg btn-danger btn-block">Za mało osób</button></td>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <td><button type="button"  data-idcourse="${type.courses.idCourse}" data-idsubject="${type.subjects.id}" class="btn btn-lg btn-success btn-block" >Dodaj grupe</button></td>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:if>
                                                    </c:forEach>
                                                </tr>
                                                </c:forEach>
                                            </c:forEach>

                                </tbody>
                            </table>
                            <!-- /.table-responsive -->

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">

                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->

            <!-- /.row -->

            <!-- /.row -->

                <!-- /.col-lg-6 -->

                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    <div id="notification" class="notification-modal">
        <div id="notification-content-id">
            <span class="close">&times;</span>
            <span id="message-notification"></span>
        </div>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../resources/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../resources/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../resources/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../resources/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../resources/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../resources/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../resources/dist/js/sb-admin-2.js"></script>
    <script type="text/javascript" src="../resources/js/managmentGroups.js"></script>
    <script type="text/javascript" src="../resources/js/learning-agreement.js"></script>
    <script type="text/javascript" src="../resources/js/notification.service.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
    <script>
        $(document).ready(function(){
            checkCreatedGroups();
        });
    </script>

</body>

</html>
